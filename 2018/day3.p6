use lib <lib>;
use Utils;

class Point { has $.x; has $.y; method gist { "{$.x}×{$.y}"  }}

class Fabric {
    has $.left;
    has $.top;
    has $.width;
    has $.height;

    method points {
	gather {
	    my @rx = $.left ..^ ($.left+$.width);
	    my @ry = ($.top) ..^ ($.top+$.height);
	    for @rx -> $x {
		for @ry -> $y {
		    take "$x,$y";
		}
	    }
	}
    }
}

sub infix:<ð>(Fabric $f1, Fabric $f2) {
    say $f1.points;
    say $f2.points;
    say '';
    my $a = set($f1.points);
    my $b = set($f2.points);
    say $a (&) $b;
}

sub pt1($data --> Int) {
    my @fabric = $data.lines.map({
my $m = Day3.parse($_);
Fabric.new(
    left => $m<position><left>,
    top => $m<position><top>,
    width => $m<size><width>,
    height => $m<size><height>
)});

    for @fabric.combinations(2) -> @f {
	@f[0] ð @f[1]
    }

    return 0;
}

sub MAIN("test") {
    my @data = q:to/E/;
    #1 @ 1,3: 4x4
    #2 @ 3,1: 4x4
    #3 @ 5,5: 2x2
    E
    my $sq = pt1(@data);

    die "Got $sq instead 4" if $sq != 4;
}
