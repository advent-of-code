
sub day1(@input) {
    my $freq = @input.sum;
    say "Frequency: $freq";
}

sub day1-pt2(@input) {
    my $seen = SetHash.new;
    my $freq = 0;
    LOOP: loop {
        for @input -> $val {
            $freq += $val;
            if $freq ∈ $seen {
                last LOOP;
            }
            $seen{$freq} = True;
        }
    }
    say "Frequency: $freq";
                
}

my @input = $*IN.lines.map(*.Int);
day1(@input);
day1-pt2(@input);
