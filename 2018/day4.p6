#`[
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
]

use Grammar::Tracer;

grammar GuardLog {
    token TOP { [ <guard-log> ]+ }
    rule guard-log {
	<shift-start> 'Guard #' <id> begins shift
	[ <log> ]+
    }
    rule log { <date> <action> }
    token shift-start { <date> }
    token id { \d+ }
    token date { '[' \d ** 4 '-' \d ** 2 '-' \d ** 2 ' ' <hour>  ']'}
    token hour { \d ** 2 ':' \d ** 2 }

    rule action {
	| 'falls asleep'
	| 'wakes up'
    }
}

my $log = GuardLog.parse(q:to/EOF/);
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
EOF

say $log<guard-log>[0]<log>.elems;

