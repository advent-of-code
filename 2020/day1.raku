```
#!/usr/bin/env raku
use v6;

sub find-entries(@lines, Int :$entries = 2) {
    @lines.combinations($entries).first(*.sum == 2020)
}

sub total-multiplied(@lines, Int :$entries = 2) {
    return [*] $_ with find-entries(@lines, :$entries);
}

multi sub MAIN(Str $filename where *.IO.f, :$entries = 2) {
    say total-multiplied($filename.IO.lines, :$entries);
}

# Run tests with `raku --doc -c day1.raku`
multi sub MAIN('test') {
    use Test;
    my @input = 1721, 979, 366, 299, 675, 1456;

    subtest '1.a', {
        is find-entries(@input), (1721, 299), 'find-entries';
        is total-multiplied(@input), 514579, 'Example';
    }
    subtest '1.b', {
        is find-entries(@input, :3entries), (979, 366, 675), 'Entries';
        is total-multiplied(@input, :3entries), 241861950, 'Example';
    }
    done-testing;
}
```
