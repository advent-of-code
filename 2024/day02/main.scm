#!/usr/bin/guile -s
!#

(add-to-load-path (format #f "~a/2024" (getcwd)))
(add-to-load-path (format #f "~a/.." (dirname (current-filename))))

(use-modules
 (aoc-utils)
 (srfi srfi-1)
 (srfi srfi-9)
 (ice-9 regex))

(define (all-decresing? report)
  (equal? report (sort report >)))

(define (all-incrising? report)
  (equal? report (sort report <)))

(define (safe-head? report)
  (if (>=(length report) 2)
      (let ((diff (abs (- (first report) (second report)))))
        (and (<= diff 3) (>= diff 1)))
      #t))

(define (safe-difference? report)
  (cond ((not (safe-head? report)) #f)
        ((null? report) #t)
        (else (safe-difference? (cdr report)))))

(define (safe-report? report)
  (and (or (all-incrising? report)
           (all-decresing? report))
       (safe-difference? report)))

(define (part1 lines)
  (let ((reports (lines->number-list lines)))
    (fold + 0 (map (lambda (report) (if (safe-report? report)
                                        1
                                        0)) reports))))

(define (indexes report)
  (let ((retval '()))
    (do ((i (1- (length report)) (1- i)))
        ((< i 0))
      (set! retval (cons i retval)))
    retval))

(define (gen-reports report)
  (cons report (map (lambda (idx)
                      (remove-nth report idx)) (indexes report))))

(define (safe-report-with-toleration? report)
  (any safe-report? (gen-reports report)))

(define (part2 lines)
  (let ((reports (lines->number-list lines)))
    (apply + (map (lambda (r)
                    (if (safe-report-with-toleration? r)
                        1
                        0)) reports))))

(define (main)
  (let ((lines (file->list "input.txt")))
    (display "Part1: ")
    (display (part1 lines))
    (newline)
    (display "Part2: ")
    (display (part2 lines))
    (newline)))

(main)
