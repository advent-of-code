#!/usr/bin/guile -s
!#

(add-to-load-path (format #f "~a/2024" (getcwd)))
(add-to-load-path (format #f "~a/.." (dirname (current-filename))))

(use-modules
 (aoc-utils)
 (srfi srfi-1)
 (srfi srfi-9)
 (ice-9 regex))

(define mul-rx (make-regexp "mul\\(([0-9]+),([0-9]+)\\)"))

(define (mul str)
  (let ((match (regexp-exec mul-rx str)))
    (* (string->number (match:substring match 1))
       (string->number (match:substring match 2)))))

(define (not-corrupted-mults memory)
  (map match:substring (list-matches mul-rx memory)))

(define (part1 memory)
  (apply + (map mul (not-corrupted-mults memory))))

(define or-rx (make-regexp "do\\(\\)|don't\\(\\)|mul\\([0-9]+,[0-9]+\\)"))

(define (part2 memory)
  (let ((tokens (map match:substring (list-matches or-rx memory)))
        (enabled? #t))
    (apply + (map (lambda (match)
                    (cond ((string= "do()" match) (begin
                                                    (set! enabled? #t)
                                                    0))
                          ((string= "don't()" match) (begin
                                                       (set! enabled? #f)
                                                       0))
                          (else (if enabled?
                                    (mul match)
                                    0))))
                  tokens))))

(define (main)
  (let ((memory (file->str "input.txt")))
    (display "Part1: ")
    (display (part1 memory))
    (newline)
    (display "Part2: ")
    (display (part2 memory))
    (newline)))

(main)
