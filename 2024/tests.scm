;(add-to-load-path "2024/")
(use-modules (aoc-utils)
             (srfi srfi-64))

(test-begin "aoc-utils")

(test-group "matrix"
  (define test-lst '((1 2 3)
                     (4 5 6)
                     (7 8 9)))

  (test-equal (list-flatten test-lst) '(1 2 3 4 5 6 7 8 9))

  (define m (make-matrix test-lst))

  (test-equal (matrix-inner m) (list-flatten test-lst))
  (test-equal (matrix-width m) 3)
  (test-equal (matrix-height m) 3)

  (test-equal (matrix-ref m 0 0) 1)
  (test-equal (matrix-ref m 1 1) 5)
  (test-equal (matrix-ref m 2 2) 9)

  (test-assert (matrix-valid-x? m 0))
  (test-assert (matrix-valid-x? m 1))
  (test-assert (matrix-valid-x? m 2))
  (test-assert (not (matrix-valid-x? m 3)))
  (test-assert (not (matrix-valid-x? m -1)))

  (test-assert (matrix-valid-y? m 0))
  (test-assert (matrix-valid-y? m 1))
  (test-assert (matrix-valid-y? m 2))
  (test-assert (not (matrix-valid-y? m 3)))
  (test-assert (not (matrix-valid-y? m -1)))
  )

(test-end "aoc-utils")
