#[derive(Debug)]
struct Number {
    position: (usize, usize),
    string: String,
}

impl Number {
}

fn main() {
    let input = include_str!("./sample.txt");
    let v: Vec<Number> = input.lines()
        .map(|line: &str| {
            let numbers: Vec<&str> = line
                .split(|c: char| { !c.is_alphanumeric() })
                .filter(|v| { v.len() > 0 })
                .collect();
            numbers.iter().enumerate().map(|(idx, number)| {
                line.match_indices(number).map(|(pos, string)| -> &mut Number {
                    &mut Number {
                        position: (idx, pos),
                        string: String::from(string),
                    }
                }).collect()
            }).collect()
        })
        .fold(vec!(), |acc: Vec<Number>, e| { acc.append(e) });
    dbg!(v);
}

#[cfg(test)]
mod test {
    use super::*;

    fn test_input() {
        let _input = include_str!("./sample.txt");
    }
}
