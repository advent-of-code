fn main() {
    let input = include_str!("./input.txt");
    let output: u32 = input.lines().map(process_line).sum();
    println!("Result {}", output);
}

fn process_line(s: &str) -> u32 {
    let mut string = String::new();
    let iter: Vec<char> = s.chars().filter(|c| {
        c.to_digit(10).is_some()
    }).map(|c| c).collect();
    assert!(iter.len() >= 1);
    let chars = [iter.first(), iter.last()];
    for char in chars {
	if let Some(c) = char {
            string.push(*c);
	}
    }
    string.parse::<u32>().unwrap()
}

#[test]
fn input_1() {
    let result = process_line("1abc2");
    assert_eq!(result, 12);
}

#[test]
fn input_2() {
    let result = process_line("pqr3stu8vwx");
    assert_eq!(result, 38);
}

#[test]
fn input_3() {
    let result = process_line("a1b2c3d4e5f");
    assert_eq!(result, 15);
}

#[test]
fn input_4() {
    let result = process_line("treb7uchet");
    assert_eq!(result, 77);
}
