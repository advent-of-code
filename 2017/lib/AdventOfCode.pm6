use v6;

sub do-captcha(@input, $cmp-offset) returns Int {
    my @tmp = @input.rotate($cmp-offset);
    (@input Z @tmp).map(-> $z {
        if $z[0] == $z[1] { $z[0] } else { 0 }
    }).reduce(&[+])
}

sub day01(Str $input) returns Int is export {
    do-captcha($input.comb, 1)
}

sub day01b(Str $input) returns Int is export {
    do-captcha($input.comb, $input.comb.elems/2)
}

sub checksum(@input) returns Int is export {
    [+] @input.map(-> @i {
        @i.max - @i.min
    })
}

sub find-divisible(@row) returns Int {
    for @row -> $x {
        for @row -> $y {
            if $x %% $y and $x != $y {
                return ($x/$y).Int
            }
        }
    }
}

sub evenly-divisible-values(@input) returns Int is export {
    [+] @input.map(-> @i {
        find-divisible(@i)
    })
}

# Day 3

sub position(Int $num) {
    my Int $num-square = (^Inf).map(*²).grep(* ≥ $num)[0];
    my Int $square-size = $num-square.sqrt.Int;
    my $max-step = ($square-size/2).floor;
    my @retval = $max-step, $max-step;
    say "$num, $num-square, $square-size, $max-step, {@retval}";
    if $num-square == $num { return @retval; }
    
    while $num-square > $num {
        say "$num-square > ($square-size - 1)";
        $num-square -= ($square-size -1);
    }

    $num-square > $num or die(" Error !! $num-square > $num");

    if $num-square == $num { return @retval; }

    for ^($square-size*4 -1) -> $i {
        $num-square -= 1;
        say "Step 2-$i: $num-square == $num ? {@retval[0]}";
        if $num-square != $num {
            @retval[0] = (@retval[0]-1) % ($max-step + 1);
        }
    }
    say "Returning: {@retval}";
    return @retval;
}

sub manhattan-distance(@position) returns Int {
    say "Sum {@position.perl}";
    [+] @position
}

sub spiral-memory(Int $input) returns Int is export {
    say "position: {position($input).perl}";
    manhattan-distance(position($input))
}
