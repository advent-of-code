use v6;
use Test;
use AdventOfCode;

plan 2;

# First part
my @mat = (
    <5 1 9 5>,
    <7 5 3>,
    <2 4 6 8>,
);

is checksum(@mat), 18;

# Second part
my @spreadsheet2 = (
    <5 9 2 8>,
    <9 4 7 3>,
    <3 8 6 5>,
);

is evenly-divisible-values(@spreadsheet2), 9;
