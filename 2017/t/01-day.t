use v6;
use Test;
use AdventOfCode;

plan 4 + 5;

is day01('1122'), 3;
is day01('1111'), 4;
is day01('1234'), 0;
is day01('91212129'), 9;


is day01b('1212'), 6;
is day01b('1221'), 0;
is day01b('123425'), 4;
is day01b('123123'), 12;
is day01b('12131415'), 4;
