

TEST_S = <<-TEST
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
TEST

def elves_food(input_str)
  return input_str.split(/^$/).map do
    |elf_rations|
    elf_rations.strip.split("\n").map(&:to_i)
  end
end

def calories_by_elf(input_str)
  elves_food(input_str).map(&:sum)
end

def max_calories(input_str)
  calories_by_elf(input_str).max
end

def top_3_calories(input_str)
  calories_by_elf(input_str).sort.reverse.take(3).sum
end

def day1
  File.open('day1.txt') do |f|
    input_str = f.read
    puts "part1: #{max_calories(input_str)}"
    puts "part2: #{top_3_calories(input_str)}"
  end
end

day1()
#puts elves_food(TEST_S)
