TEST_S = <<EOF
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
EOF

class Stacks

end

class Instructions
  attr_reader :indexes

  def initialize(header)
    indexes = {}
    indexes.default = []
    header.readlines do |line|
      line.scan(/\w/).each do |c|
        indexes[line.index(c)] << c
      end
    end

    self.indexes = {}

    indexes.each_key.each_with_index do |i, index|
      self.indexes[i] = indexes[index]
    end
  end
end

def parse_input(filename)
  File.open(filename) do |file|
    contents = file.read
    header, body = contents.split(/^$/)
  end

end
