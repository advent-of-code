require 'pry'

class Array
  def each_n(n)
    while not self.empty?
      a, b, c = self.shift(n)
      yield [a, b, c]
    end
  end
end
