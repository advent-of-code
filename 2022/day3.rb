require 'set'

def split_in_half(s)
  pivot = s.length/2
  return [s[0, pivot], s[pivot, s.length]]
end

def shared_item(s)
  rucksack1, rucksack2 = split_in_half(s)
  tmp = (rucksack1.chars.to_set & rucksack2.chars.to_set)
  puts tmp
  return tmp.first
end

def scores_hash
  a = ('a'..'z').zip((1..26)) + ('A'..'Z').zip((27..52))
  a.to_h
end

TEST_S = <<-EOC
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
EOC

def test_1(s)
  s.lines.map do |line|
    item = shared_item(line)
    scores_hash[item]
  end.sum
end

def day1

  puts test_1(TEST_S)

  File.open('./day3.txt') do |f|
    retval = f.readlines.map do |line|
      item = shared_item(line)
      scores_hash[item]
    end
    puts retval.sum
  end
end

require './utils'

def day2
  badges = []
  File.open('./day3.txt') do |f|

    f.readlines.each_n(3) do |a,b,c|
      badge = a.chars.to_set & b.chars.to_set & c.chars.to_set
      badges << scores_hash[badge.first]
    end
  end
  puts badges.sum
end

day2()
