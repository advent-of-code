TEST_S = <<-EOF
A Y
B X
C Z
EOF

def score(picked_item, outcome)
  fail "Wrong picked item '#{picked_item}'" unless picked_item
  outcome_score = case outcome
                  when :win
                    6
                  when :draw
                    3
                  when :lose
                    0
                  end
  picked_score = case picked_item
                 when /A|X/
                   1
                 when /B|Y/
                   2
                 when /C|Z/
                   3
                 end
  #puts "#{outcome_score} + #{picked_score}"
  return outcome_score + picked_score
end

def guessed_strategy(line)
  outcome_map = {
    'A X' => :draw,
    'A Y' => :win,
    'A Z' => :lose,
    'B X' => :lose,
    'B Y' => :draw,
    'B Z' => :win,
    'C X' => :win,
    'C Y' => :lose,
    'C Z' => :draw
  }
  return outcome_map[line]
end

def correct_strategy(item1, item2)
  return :win if item2 == 'Z'
  return :draw if item2 == 'Y'
  return :lose if item2 == 'X'
end

def what_to_choose(oponent_shape, outcome)
  case outcome
  when :draw
    return oponent_shape
  when :win
    case oponent_shape
    when 'A'
      return 'B'
    when 'B'
      return 'C'
    when 'C'
      return 'A'
    else
      fail "Don't know this shape '#{oponent_shape}'"
    end
  when :lose
    case oponent_shape
    when 'A'
      return 'C'
    when 'B'
      return 'A'
    when 'C'
      return 'B'
    else
      fail "Don't know this shape '#{oponent_shape}'"
    end
  else
    fail "Don't know this outcome '#{outcome}'"
  end
end

def part1(filename)
  score = 0
  File.open(filename) do |f|
    scores = f.readlines.map do |line|
      item1, item2 = line.chomp.split()
      outcome = guessed_strategy(line.chomp)
      score(item2, outcome)
    end

    score = scores.sum
  end
  score
end

def part2(filename)
  score = 0
  File.open(filename) do |f|
    scores = f.readlines.map do |line|
      item1, item2 = line.chomp.split()
      outcome = correct_strategy(item1, item2)
      shape = what_to_choose(item1, outcome)
      score(shape, outcome)
    end

    score = scores.sum
  end
  score
end

#puts part1('./day2-test.txt')
#puts part1('./day2.txt')


puts 'Part2'
puts part2('./day2-test.txt')
puts part2('./day2.txt')
